package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"app.com/calc/note"
	"app.com/calc/todo"
)

type saver interface{
	Save () error
}
type displayer interface{
	Display()
}

type outputable interface{
	saver
	displayer
}
func add[T int| float64| string](a, b T) T {
	return a + b
}
func saveData( data saver) error{
	result :=add(1,3)
	fmt.Print(result)
	err := data.Save()
	var t any
	typed, ok :=t.(int)
	if ok{
		println(typed ,"is an int")
	}
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println("Saving the data succeeded!")
	return nil
}
func outputData(data outputable) error {
	data.Display()
	return saveData(data)
}
func noteStart() {
	todoText:= getTodoData()

	todo, err:= todo.New(todoText)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = outputData( todo)
	if err != nil {
		fmt.Println(err)
		return
	}
	title, content := getNoteData()
	userNote, err := note.New(title, content)

	if err != nil {
		fmt.Println(err)
		return
	}
	err = outputData(userNote)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func getTodoData() string {

	return getUserInput("Todo text:")
}
func getNoteData() (string, string) {
	title := getUserInput("Note title:")
	content := getUserInput("Note content:")

	return title, content
}

func getUserInput(prompt string) string {
	fmt.Printf("%v ", prompt)

	reader := bufio.NewReader(os.Stdin)

	text, err := reader.ReadString('\n')

	if err != nil {
		return ""
	}

	text = strings.TrimSuffix(text, "\n")
	text = strings.TrimSuffix(text, "\r")

	return text
}
