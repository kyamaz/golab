package user
import (
	"errors"
    "fmt"
    "time"
)
type User struct{
    firstName string `json:"first_name"`
    lastName string  `json:"last_name"`
    birthdate string  `json:"birth_date"`
    createdAd time.Time  `json:"created_at"`
}

type Admin struct {
	email string
	pwd string
	User
}
func (u *User) OuputDetail(){
    fmt.Println(u.firstName, u.createdAd)
}

func (u *User) Clear(){
    u.firstName=""
    u.lastName=""
}

func New( firstName, lastName, birthdate string) (*User, error) {
    if firstName == "" || lastName =="" || birthdate ==""{
        return nil, errors.New("invalid value")
    }
    return &User{
        firstName: firstName,
        lastName:lastName,
        birthdate:birthdate,
        createdAd: time.Now(),
    }, nil
}

func NewAdmin( email, pwd string) Admin{
	return Admin{
		email:email,
		pwd:pwd,
		User:User{
			firstName:"TEST",
			lastName:"TEST",
			birthdate:"01/01/2000",
			createdAd: time.Now(),
		},
	}
}