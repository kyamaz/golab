package fileops
import (
    "errors"
    "fmt"
    "os"
    "strconv"
)

func WriteToFile(value float64, fileName string){
    balanceTxt:= fmt.Sprint(value)
    os.WriteFile(fileName,[]byte(balanceTxt), 0644 )
}
func ReadFile(fileName string) (float64, error ){
    data, err := os.ReadFile(fileName)
    if err != nil{
        fmt.Print(err)
        return 1000, errors.New("failed to find files")
    }
    balanceTxt :=string(data)
    value, parseErr := strconv.ParseFloat(balanceTxt, 64)
    if parseErr != nil{
        fmt.Print(err)
        return 1000, errors.New("failed to value")
    }

    return value, nil
}