package main

import (
    "fmt"
    "app.com/calc/user"
)
/*type alias type customString string
//add method to buildin type via alias
func ( text customString) log(){
    fmt.PrintLn(text)
}*/
func structExample() {
    firstName := getUserData("Please enter your first name: ")
	lastName := getUserData("Please enter your last name: ")
	birthdate := getUserData("Please enter your birthdate (MM/DD/YYYY): ")

    var appUser *user.User
    appUser, err := user.New( firstName, lastName, birthdate)
    if(err != nil){
     fmt.Println(err)
     return
   }
	// ... do something awesome with that gathered data!
    appUser.OuputDetail()
    appUser.Clear()
    appUser.OuputDetail()

    admin:= user.NewAdmin("test@test.com", "1234")
    admin.OuputDetail()
    //ouputDetail(&appUser)
	//fmt.Println(firstName, lastName, birthdate)
}

func getUserData(promptText string) string {
	fmt.Print(promptText)
	var value string
	fmt.Scanln(&value)
	return value
}
