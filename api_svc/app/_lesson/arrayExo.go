package main

import "fmt"


type Product struct{
	title string
	id string
	price float64
}

type FloatMap map[string] float64
func (m FloatMap) output(){
	fmt.Println(m)
}

func specialStuff(){

	userNames:=make([]string, 2, 10)//preallocate fixed size list with cap/max len

	userNames[0]= "toto"
	userNames=append(userNames, "tata")

	fmt.Println(userNames)
	courseRatings:=make(FloatMap, 5) //no empty slot, just max
	courseRatings["react"]=4.5
	courseRatings["go"]=4.9
	courseRatings["ng"]=4.6

	courseRatings.output()

	for idx, value :=range userNames{
		fmt.Println(idx,"<-->",value)
	}

	for key, val :=range courseRatings{

		fmt.Println(key,"<-->",val)
	}
}
func mapSection(){
	websites:=map[string]string{
		"google":"http://www.google.com",
		"amazon":"http://www.aws.com",
	}
	fmt.Println(websites["amazon"])
	websites["amazon"]="http://www.amazon.com"
	websites["toto"]="http://www.toto.com"
	delete(websites, "google")
	fmt.Println(websites)
}
func listSection(){

 	prices :=[]float64{10.2,11.0}
	updatedL:=append(prices, 5.23, 12.3)//copy
	fmt.Println(prices)
	prices[0]=6.66
	fmt.Println(prices)
	fmt.Println(updatedL)

	discount:=[]float64{15.2, 2.99}
	merged:=append(prices, discount...)
	fmt.Println(merged)
}

func arraySection(){
	var productNames [4]string
	productNames=[4]string{ "book"}
	productNames[2]="stuff"
 	prices :=[4]float64{ 1.0, 2.33, 20.5, 20.99}
	featuredPrices := prices[1:3]//include start, exclude end
	highlight :=featuredPrices[:1]
	fmt.Println(prices)
 	fmt.Println(productNames)
	fmt.Println(prices[2])
	fmt.Println(featuredPrices)
	fmt.Println(len(highlight))
}