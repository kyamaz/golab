package main

import (
    "fmt"
)
func pointers(){
    age :=32
    agePointer:= &age //*int type

    fmt.Println("age ", *agePointer)
    mutAdultYears(agePointer)
    fmt.Println("calc age ", age)
}

func mutAdultYears(y *int){
    *y=*y -18
}
