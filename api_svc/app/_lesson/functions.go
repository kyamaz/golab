package main

import "fmt"
type transfInt func(int) int
func doubleN (n int) int{
	return n*2
}
func tripleN (n int) int{
	return n*3
}
func transformNum( ns *[]int, transfn transfInt)[]int{
	doubleN:=[]int{}
	for _, value := range *ns{
		doubleN=append(doubleN, transfn(value) )
	}
	return doubleN
}

func functionStuff(){
	nums:=[]int{1,2,3}
	d:=transformNum(&nums, func(i int) int { return i*5 })
	for _, v := range d{
		fmt.Println(v)
	}
}

func transfFactory(factor int) func(int)int{
	return func (n int)int {
		return n * factor
	}
}