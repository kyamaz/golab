package main

import (
    "math"
    "fmt"
    "app.com/calc/fileops"
    "github.com/Pallinder/go-randomdata"
)
const balanceFile ="balance.txt"

func calcValues(investAmount, expectedReturnRate, years, inflationRate float64) (fValue float64, futureReal float64){
    fValue =investAmount * math.Pow(1+expectedReturnRate/100, years)
    futureReal = fValue / math.Pow(1+inflationRate/100, years)
    return fValue, futureReal
}
func sample(){
    const inflationRate =2.5
    var investAmount, years float64
    expectedReturnRate := 5.5
    fmt.Print("input investAmount, years and expected return rate")
    fmt.Scan( &investAmount,&years, &expectedReturnRate)
    //formattedFv := fmt.Sprintf("future value: %.2f\n", futureValue)
    //formattedRfv := fmt.Sprintf("future real: %.2f\n", futureReal)
    //fmt.Println("future value:", futureValue)
    futureValue, futureReal :=calcValues(investAmount, expectedReturnRate, years, inflationRate)
    fmt.Printf(`
    futureValue: %.2f
    future real: %.2f`, futureValue, futureReal )
    //fmt.Println("feature real:", futureReal)
    //fmt.Print(formattedFv, formattedRfv)
}
func exo1(){
    var revenue, expense, taxRate float64
    fmt.Print("please input revenue, expense, taxRate")
    fmt.Scan(&revenue, &expense, &taxRate)

    ebt := revenue -expense
    profit := ebt * (1- taxRate/100)
    ratio := ebt /profit

    fmt.Println( ebt, profit, ratio)

}

func banking(){
    accountBalance, err:=fileops.ReadFile(balanceFile)
    if err != nil {
        fmt.Println(err)
        fmt.Println("something went wrong")
        //panic(err)
    }
    fmt.Println("Welcome to go bank.\n what you wanna do")
    fmt.Println("reach us",randomdata.PhoneNumber())
    for {
    greedOpt()
    var choice int
    fmt.Print("choose action: ")
    fmt.Scan(&choice)
    switch choice {
        case 1:{
            fmt.Println(accountBalance)
        }
        case 2:{
            var deposit float64
            fmt.Scan(&deposit)
            if deposit <= 0 {
                fmt.Print("insufficient deposit")
                continue
            }

            accountBalance += deposit
            fmt.Println(accountBalance)

        }
        case 3: {
            var draw float64
            fmt.Scan(&draw)
            if draw>accountBalance{
                fmt.Print("insufficient balance")
                continue
            }
            accountBalance -= draw
            fmt.Println(accountBalance)
        }
        case 4:{
            fileops.WriteToFile(accountBalance, balanceFile)
            fmt.Print("bye")
            fmt.Print("ty")
            return
        }
        default:{
            fmt.Print("Please select valid choice")
        }
    }

    }
}
