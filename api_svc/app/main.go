package main

import (
	"ky/simpleapi/db"

	"ky/simpleapi/routes"

	"github.com/gin-gonic/gin"
)

func main() {

	loadEnv()
	db.Init()

	server := gin.Default()

	routes.RegisterRoute(server)

	checkPort()
	server.Run("0.0.0.0:5300")
}
