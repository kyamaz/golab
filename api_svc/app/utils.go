package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

func checkPort() {
	listener, err := net.Listen("tcp", ":5300")

	if err != nil {
		fmt.Println("port in use")

		cmd := exec.Command("bash", "-c", "lsof -t -i :5300")
		output, _ := cmd.Output()
		pidStr := strings.TrimSpace(string(output))
		pid, _ := strconv.Atoi(pidStr)

		process, _ := os.FindProcess(pid)
		processErr := process.Kill()
		if processErr != nil {
			fmt.Printf("failed to kill process %b", pid)
			return
		}
	} else {
		listener.Close()
	}

}

func loadEnv() {

	if err := godotenv.Load("./_env/.env"); err != nil {
		log.Fatal("failed to load env ")
	}

}
