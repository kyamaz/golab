package auth

import (
	"errors"
	"time"

	"os"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
)

func HashPwd(pwd string) (string, error) {

	bytes, err := bcrypt.GenerateFromPassword([]byte(pwd), 4)

	return string(bytes), err
}

func CheckPwd(pwd, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pwd))

	return err == nil

}

func getSecret() (string, error) {
	jwtSecret := os.Getenv("JWT_SECRET")

	if jwtSecret == "" {
		return "", errors.New("secret missing")
	}

	return jwtSecret, nil
}
func GenJwtToken(email, userId string) (string, error) {

	secret, jErr := getSecret()
	if jErr != nil {
		return "", jErr
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":  email,
		"userId": userId,
		"exp":    time.Now().Add(time.Hour * 4).Unix(),
	})
	return token.SignedString([]byte(secret))
}

func verifyJWT(token string) (string, error) {

	secret, jErr := getSecret()
	if jErr != nil {
		return "", jErr
	}

	parseToken, pErr := jwt.Parse(token, func(tk *jwt.Token) (interface{}, error) {
		_, ok := tk.Method.(*jwt.SigningMethodHMAC)

		if !ok {

			return nil, errors.New("failed to parse token")
		}

		return []byte(secret), nil
	})
	if pErr != nil {
		return "", pErr
	}

	if !parseToken.Valid {

		return "", errors.New("invalid token")
	}

	claims, ok := parseToken.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid claim")
	}
	uId := claims["userId"].(string)

	return uId, nil

}
