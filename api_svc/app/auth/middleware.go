package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Protected(ctx *gin.Context) {

	tokenCookie, tErr := ctx.Request.Cookie("authorization")
	if tErr != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": "authorization missing"})
		return
	}

	userId, err := verifyJWT(tokenCookie.Value)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": err.Error()})
		return
	}

	ctx.Set("userId", userId)

	ctx.Next()
}
