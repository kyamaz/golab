package routes

import (
	"ky/simpleapi/pkg/user"

	"ky/simpleapi/auth"
	"ky/simpleapi/pkg/todo"

	"github.com/gin-gonic/gin"
)

func RegisterRoute(server *gin.Engine) {

	apiPrefix := "api/v1/"
	// PUBLIC
	server.POST(apiPrefix+"user", user.CreateUser)
	server.POST(apiPrefix+"user/signin", user.Signin)

	server.GET(apiPrefix+"todo", todo.Fetch)
	//PROTECTED
	protected := server.Group("/")
	protected.Use(auth.Protected)

	protected.POST(apiPrefix+"todo", todo.Create)
}
