package models

import (
	"context"
	"errors"
	"ky/simpleapi/auth"
	"ky/simpleapi/db"

	"github.com/google/uuid"
)

type User struct {
	ID       string
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,gte=5"`
}

func (user *User) Save() error {

	hashPassword, pErr := auth.HashPwd(user.Password)

	if pErr != nil {

		return pErr

	}

	var userId []byte

	query := "INSERT INTO users(email, password) VALUES($1,$2) RETURNING id;"
	qErr := db.DB.QueryRow(context.Background(), query, user.Email, hashPassword).Scan(&userId)
	if qErr != nil {
		return qErr
	}

	uuidObj, uErr := uuid.FromBytes(userId)

	if uErr != nil {

		return uErr
	}
	user.Password = hashPassword
	user.ID = uuidObj.String()
	return nil
}

func (user *User) Validate() error {
	query := "SELECT id, password FROM users WHERE email = $1;"
	var _pwd string
	qErr := db.DB.QueryRow(context.Background(), query, user.Email).Scan(&user.ID, &_pwd)
	if qErr != nil {
		return qErr
	}
	if isValid := auth.CheckPwd(user.Password, _pwd); !isValid {

		return errors.New("invalid creds")
	}

	return nil

}

func ValidateUserId(userId string) bool {
	query := "SELECT EXISTS(SELECT 1 FROM users WHERE id = $1)::int;"
	var exists []byte
	qErr := db.DB.QueryRow(context.Background(), query, userId).Scan(&exists)
	if qErr != nil {
		return false
	}
	return string(exists) != "0"
}
