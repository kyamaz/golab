package models

import (
	"ky/simpleapi/db"

	"context"
)

type Todo struct {
	ID          int64
	Title       string
	Description string
	UserID      string
}

func FetchAll() ([]Todo, error) {
	query := `SELECT id, title, description, user_id FROM todos;`
	rows, qErr := db.DB.Query(context.Background(), query)

	if qErr != nil {

		return nil, qErr
	}

	defer rows.Close()

	todos := []Todo{}

	for rows.Next() {
		var todo Todo

		if err := rows.Scan(&todo.ID, &todo.Title, &todo.Description, &todo.UserID); err != nil {

			return nil, err
		}
		todos = append(todos, todo)

	}

	return todos, nil
}

func (todo *Todo) Save() error {

	query := `
		INSERT INTO todos (title, description, user_id)
		VALUES( $1, $2, $3)
		RETURNING id;
	`
	var todoId []byte
	qErr := db.DB.QueryRow(context.Background(), query, todo.Title, todo.Description, todo.UserID).Scan(&todoId)

	if qErr != nil {
		return qErr
	}

	parsedTodoId, pErr := getIdFromQuery(todoId)

	if pErr != nil {
		return pErr
	}
	todo.ID = parsedTodoId

	return nil

}
