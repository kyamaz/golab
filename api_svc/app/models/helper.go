package models

import (
	"strconv"
)

// query is returned as byte []
// so covert it to int64 (db id type)
func getIdFromQuery(idQ []byte) (int64, error) {
	_int, err := strconv.ParseInt(string(idQ), 10, 64)

	if err != nil {
		return 0, err
	}

	return _int, nil
}
