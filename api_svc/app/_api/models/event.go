package models

import (
	"errors"
	"time"

	"goapi/events/db"
)

type Event struct {
	ID          int64
	Name        string    `binding:"required"`
	Description string    `binding:"required"`
	Location    string    `binding:"required"`
	DateTime    time.Time `binding:"required"`
	UserID      int64
}

func (e *Event) Save() error {
	query := `
		INSERT INTO events (name, description, location, dateTime, user_id)
		VALUES(?,?,?,?,?)
	`
	stmt, err := db.DB.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	results, err := stmt.Exec(e.Name, e.Description, e.Location, e.DateTime, e.UserID)
	if err != nil {
		return err
	}
	id, err := results.LastInsertId()
	e.ID = id
	return err
}

func GetAllEvents() ([]Event, error) {
	query := "SELECT * from events"

	rows, err := db.DB.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var events []Event
	for rows.Next() {
		var event Event
		err := rows.Scan(&event.ID, &event.Name, &event.Description, &event.Location, &event.DateTime, &event.UserID)
		if err != nil {
			return nil, err
		}
		events = append(events, event)
	}
	return events, nil
}

func GetEventDetail(id int64) (*Event, error) {
	query := "SELECT * FROM events where id = ?"
	row := db.DB.QueryRow(query, id)
	var event Event
	err := row.Scan(&event.ID, &event.Name, &event.Description, &event.Location, &event.DateTime, &event.UserID)
	if err != nil {
		return nil, err
	}
	return &event, nil
}

func (ev Event) Update() error {
	query := `
		UPDATE events
		SET name=?, description=?, location=?, dateTime=?
		WHERE id= ?
	`

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, errStmt := stmt.Exec(ev.Name, ev.Description, ev.Location, ev.DateTime, ev.ID)
	return errStmt
}

func DeleteEvent(id int64) error {
	query := `
		DELETE FROM events
		where id = ?
	`

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, errStmt := stmt.Exec(id)
	return errStmt
}

func (ev Event) Register(userId int64) error {

	query := `
		INSERT INTO registrations (event_id, user_id)
		VALUES(?,?)
	`
	stmt, qErr := db.DB.Prepare(query)
	if qErr != nil {
		return qErr
	}
	defer stmt.Close()

	_, err := stmt.Exec(ev.ID, userId)
	return err
}

func (ev Event) DeleteRegister(userId int64) error {

	query := `
		DELETE FROM registrations
		where user_id=? AND event_id=?
	`
	stmt, qErr := db.DB.Prepare(query)

	if qErr != nil {
		return qErr
	}
	defer stmt.Close()

	q, err := stmt.Exec(userId, ev.ID)

	if err != nil {
		return err
	}
	nDel, delErr := q.RowsAffected()

	if err != delErr || nDel == 0 {
		return errors.New("nothing happened")
	}
	return nil
}
