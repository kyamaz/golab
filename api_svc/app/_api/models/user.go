package models

import (
	"errors"
	"goapi/events/db"
	"goapi/events/utils"
)

type User struct {
	ID       int64
	Email    string `binding:"required"`
	Password string `binding:"required"`
}

func (u *User) Save() error {
	query := "INSERT INTO users(email, password) values(?,?)"

	stmt, err := db.DB.Prepare(query)

	if err != nil {
		return err
	}
	defer stmt.Close()

	hashPwd, pwd_err := utils.HashPwd(u.Password)

	if pwd_err != nil {
		return pwd_err
	}
	result, st_err := stmt.Exec(u.Email, hashPwd)

	if st_err != nil {
		return st_err
	}

	userId, user_err := result.LastInsertId()

	u.ID = userId
	u.Password = hashPwd

	return user_err
}

func (u *User) ValidateCreds() error {

	query := `	SELECT id, password 
						FROM users
						WHERE email = ? 
		
	`

	row := db.DB.QueryRow(query, u.Email)
	var userPwd string
	dbErr := row.Scan(&u.ID, &userPwd)

	if dbErr != nil {
		return errors.New("invalid creds 1")
	}
	isPwdValid := utils.CheckPassword(u.Password, userPwd)

	if !isPwdValid {

		return errors.New("invalid creds 2")
	}
	return nil
}
