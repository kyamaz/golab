package utils

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

const SECRET = "secure"

func GenToken(email string, userId int64) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":  email,
		"userId": userId,
		"exp":    time.Now().Add(time.Hour * 3).Unix(),
	})

	return token.SignedString([]byte(SECRET))
}

func VerifyToken(tk string) (int64, error) {
	parsedTk, p_err := jwt.Parse(tk, func(token *jwt.Token) (interface{}, error) {

		_, ok := token.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, errors.New("invalid token")
		}
		return []byte(SECRET), nil
	})
	if p_err != nil {
		return 0, p_err
	}
	isValidTk := parsedTk.Valid
	if !isValidTk {
		return 0, errors.New("invalid tk 2")
	}

	claims, ok := parsedTk.Claims.(jwt.MapClaims)
	if !ok {
		return 0, errors.New("invalid claims")
	}

	userId := int64(claims["userId"].(float64))

	return userId, nil
}
