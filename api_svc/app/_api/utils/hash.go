package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func HashPwd(str string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(str), 14)

	if err != err {
		return "", err
	}

	return string(bytes), nil
}

func CheckPassword(pwd, hashed string) bool {

	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(pwd))
	return err == nil

}
