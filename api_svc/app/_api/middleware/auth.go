package middleware

import (
	"goapi/events/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Auth(ctx *gin.Context) {

	token := ctx.Request.Header.Get("Authorization")
	if token == "" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "not auth"})
		return
	}
	userId, tk_err := utils.VerifyToken(token)
	if tk_err != nil {

		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": tk_err})
		return
	}

	ctx.Set("userId", userId)
	ctx.Next()

}
