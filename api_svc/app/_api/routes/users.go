package routes

import (
	"fmt"
	"goapi/events/models"
	"goapi/events/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func signup(ctx *gin.Context) {
	var user models.User
	err := ctx.ShouldBindJSON(&user)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err})
		return
	}

	u_err := user.Save()

	if u_err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong"})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "success", "data": user})
}

func signin(ctx *gin.Context) {

	var user models.User

	payload_err := ctx.ShouldBindJSON(&user)

	if payload_err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": payload_err})
	}

	uErr := user.ValidateCreds()
	if uErr != nil {
		ctx.JSON(http.StatusUnauthorized, gin.H{"message": uErr})
		return
	}
	fmt.Println(user)
	token, tkErr := utils.GenToken(user.Email, user.ID)
	fmt.Println(token, tkErr)

	if tkErr != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": tkErr})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "success", "data": token})
}
