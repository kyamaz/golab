package routes

import (
	"fmt"
	"goapi/events/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func createRegistration(ctx *gin.Context) {

	userId := ctx.GetInt64("userId")
	eventId, evErr := strconv.ParseInt(ctx.Param("eventId"), 10, 64)

	if evErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"data": "bad request"})
		return
	}

	ev, eErr := models.GetEventDetail(eventId)

	if eErr != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"data": eErr})
		return
	}

	rErr := ev.Register(userId)
	if rErr != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"data": rErr})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"data": "event registered"})

}
func deleteRegistration(ctx *gin.Context) {

	userId := ctx.GetInt64("userId")
	eventId, evErr := strconv.ParseInt(ctx.Param("eventId"), 10, 64)

	if evErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"data": "bad request"})
		return
	}
	var event models.Event
	event.ID = eventId
	eErr := event.DeleteRegister(userId)

	if eErr != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": eErr.Error()})
		return
	}
	msg := fmt.Sprintf("event %s was deleted", strconv.FormatInt(eventId, 10))
	ctx.JSON(http.StatusOK, gin.H{"message": msg})
}
