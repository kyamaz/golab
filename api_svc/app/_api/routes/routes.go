package routes

import (
	"goapi/events/middleware"

	"github.com/gin-gonic/gin"
)

func RegisterRoute(server *gin.Engine) {
	/* public  */
	server.POST("/signup", signup)
	server.POST("/login", signin)
	server.GET("/events", getEvents)

	/*  protected  */

	protected := server.Group("/")
	protected.Use(middleware.Auth)

	protected.GET("/events/:eventId", getEvent)
	protected.POST("/events", createEvent)

	protected.POST("/events/:eventId/register", createRegistration)
	protected.DELETE("/events/:eventId/register", deleteRegistration)

	protected.PUT("/events/:eventId", updateEvent)
	protected.DELETE("/events/:eventId", deleteEvent)

}
