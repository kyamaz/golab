package routes

import (
	"net/http"
	"strconv"

	"goapi/events/models"

	"github.com/gin-gonic/gin"
)

func getEvents(ctx *gin.Context) {
	events, err := models.GetAllEvents()
	if err != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": events})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": events})
}

func getEvent(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("eventId"), 10, 64)
	if err != nil {

		ctx.JSON(http.StatusBadRequest, gin.H{"message": "invalid id"})
		return
	}
	event, err := models.GetEventDetail(id)
	if err != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": event})
}

func createEvent(ctx *gin.Context) {
	var event models.Event
	err := ctx.ShouldBindJSON(&event)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err})
		return
	}

	event.UserID = ctx.GetInt64("userId")
	err = event.Save()
	if err != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	ctx.JSON(http.StatusCreated, gin.H{"message": "event created", "data": event})
}

func updateEvent(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("eventId"), 10, 64)
	if err != nil {

		ctx.JSON(http.StatusBadRequest, gin.H{"message": "invalid id"})
		return
	}
	event, err := models.GetEventDetail(id)

	if err != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}

	if event.UserID != ctx.GetInt64("userId") {

		ctx.JSON(http.StatusUnauthorized, gin.H{"message": "invalid auth user id"})
		return
	}

	var updatedEvent models.Event
	err = ctx.ShouldBindJSON(&updatedEvent)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err})
		return
	}

	updatedEvent.ID = event.ID
	updatedEvent.UserID = event.UserID

	errUpdate := updatedEvent.Update()
	if errUpdate != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": updatedEvent})
}

func deleteEvent(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("eventId"), 10, 64)
	if err != nil {

		ctx.JSON(http.StatusBadRequest, gin.H{"message": "invalid id"})
		return
	}
	event, errId := models.GetEventDetail(id)
	if errId != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": errId})
		return
	}
	if event.UserID != ctx.GetInt64("userId") {

		ctx.JSON(http.StatusUnauthorized, gin.H{"message": "invalid auth user id"})
		return
	}

	errDB := models.DeleteEvent(id)
	if errDB != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "event " + ctx.Param("eventId") + " was deleted"})
}
