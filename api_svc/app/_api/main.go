package main

import (
	"goapi/events/db"

	"goapi/events/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	db.Init()

	server := gin.Default()

	routes.RegisterRoute(server)

	server.Run("0.0.0.0:5300")
}
