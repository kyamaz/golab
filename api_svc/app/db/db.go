package db

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v5/pgxpool"
)

var DB *pgxpool.Pool

func Init() {

	connStr := "postgresql://" + os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASS") + "@" + os.Getenv("DB_HOST") + "/" + os.Getenv("DB_NAME")
	var dbErr error
	DB, dbErr = pgxpool.New(context.Background(), connStr)

	if dbErr != nil {
		fmt.Fprintf(os.Stderr, "unable to connect to database: %v", dbErr)
		os.Exit(1)
		return
	}

	createTables()

}

func createTables() {
	loadUuid := `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`
	_, idErr := DB.Exec(context.Background(), loadUuid)

	if idErr != nil {
		fmt.Println("failed to load uuid extensions")
		panic(idErr)
	}

	createUserTables := `
		--DROP TABLE IF EXISTS users CASCADE;
		CREATE TABLE IF NOT EXISTS users(
			id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
			email VARCHAR(250) UNIQUE NOT NULL,
			password VARCHAR(250) NOT NULL,
			updated_at TIMESTAMPTZ,
			created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
		)
	`

	_, uErr := DB.Exec(context.Background(), createUserTables)

	if uErr != nil {
		fmt.Println("failed to create user table")
		panic(uErr)
	}
	createTodoTables := `
		--DROP TABLE IF EXISTS todos CASCADE;
		CREATE TABLE IF NOT EXISTS todos(
			id SERIAL PRIMARY KEY,
			title VARCHAR(200) NOT NULL,
			description TEXT,
			updated_at TIMESTAMPTZ,
			created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
			user_id UUID NOT NULL,
			CONSTRAINT fk_user
				FOREIGN KEY (user_id)
					REFERENCES users(id)
					ON DELETE CASCADE
			)

	`
	_, err := DB.Exec(context.Background(), createTodoTables)

	if err != nil {
		fmt.Println("failed to create todo table")
		panic(err)
	}

}
