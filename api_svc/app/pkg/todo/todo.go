package todo

import (
	"ky/simpleapi/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Fetch(ctx *gin.Context) {

	data, err := models.FetchAll()

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"msg": err.Error()})

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"msg": "success", "data": data})
}

func Create(ctx *gin.Context) {
	var todo models.Todo
	_userId, ok := ctx.Get("userId")

	if !ok {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": "user info missing"})
		return
	}

	userId, pOk := _userId.(string)

	if !pOk {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"msg": "failed to get user info"})
		return
	}
	ctx.ShouldBind(&todo)
	//parsedId, pErr := strconv.ParseInt(userId, 10, 64)

	//if pErr != nil {
	//	ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"msg": pErr.Error()})
	//	return
	//}
	if ok := models.ValidateUserId(userId); !ok {

		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": "invalid user info"})
		return

	}
	todo.UserID = userId

	if dErr := todo.Save(); dErr != nil {

		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"msg": dErr.Error()})
		return
	}

	data := make(map[string]int64, 1)
	data["id"] = todo.ID

	ctx.JSON(http.StatusCreated, gin.H{"msg": "created", "data": data})
}
