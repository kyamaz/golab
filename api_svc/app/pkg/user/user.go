package user

import (
	"ky/simpleapi/auth"
	"ky/simpleapi/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

func CreateUser(ctx *gin.Context) {

	var user models.User

	if bErr := ctx.ShouldBindJSON(&user); bErr != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": bErr.Error()})
		return
	}

	if uErr := user.Save(); uErr != nil {

		ctx.JSON(http.StatusInternalServerError, gin.H{"msg": uErr.Error()})
		return
	}

	data := gin.H{"id": user.ID}
	ctx.JSON(http.StatusCreated, gin.H{"msg": "user created", "data": data})
}

func Signin(ctx *gin.Context) {
	var user models.User

	if bErr := ctx.ShouldBindJSON(&user); bErr != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": bErr.Error()})
		return
	}

	if uErr := user.Validate(); uErr != nil {

		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": uErr.Error()})

		return
	}

	token, tkErr := auth.GenJwtToken(user.Email, user.ID)
	if tkErr != nil {

		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"msg": tkErr.Error()})
		return
	}
	hourInSecond := 3600
	ctx.SetCookie("authorization", token, hourInSecond*3, "/", "localhost", false, true)
	ctx.JSON(http.StatusOK, gin.H{"msg": "ok", "data": token})
}
